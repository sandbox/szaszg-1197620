<?php

/**
 * create the edit form.
 */

function tableutils_edit_cast_hidden($content, $element) {
  return str_replace('type="text"', 'type="hidden"', $content);
}

function tableutils_edit_form() {
  $params = drupal_get_query_parameters($_POST);

  global $base_url;
  $p = $base_url . '/' . drupal_get_path('module', 'ovsz_phonebook') . '/';
  $org = $pos = array();

  _ovsz_phonebook_load_org_pos($org, $pos);
  foreach ($org as $k => $data) {
    $optorg[$k] = $data['org'];
  }
  foreach ($pos as $k => $data) {
    $optpos[$k] = $data['pos'];
  }

  $form['ovsz-phonebook-ph']['title'] = array(
    '#markup' => '<div id="ovsz-phonebook-phone-div">',
  );
  $form['ovsz-phonebook-ph']['form'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('ovsz-phonebook-form')),
  );
  $form['ovsz-phonebook-ph']['form']['close'] = array(
    '#markup' => '<img id="ovsz-phonebook-form-close" src="' . $p . 'icon-close.png" style="float: right" />',
  );
  $form['ovsz-phonebook-ph']['form']['id'] = array(
    '#id' => 'edit-id',
    '#type' => 'textfield',
//    '#type' => 'hidden',
    '#default_value' => '',
    '#post_render' => array('_tableutils_edit_cast_hidden'),
  );
  $form['ovsz-phonebook-ph']['form']['org'] = array(
    '#type' => 'select',
    '#title' => t('Organization'),
    '#options' => $optorg,
    '#default_value' => 0,
  );
  $form['ovsz-phonebook-ph']['form']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => '',
  );
  $form['ovsz-phonebook-ph']['form']['pos'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#options' => $optpos,
    '#default_value' => 0,
  );
  $form['ovsz-phonebook-ph']['form']['email'] = array(
    '#type' => 'textarea',
    '#title' => t('e-mail'),
    '#rows' => 2,
    '#default_value' => '',
  );
  $form['ovsz-phonebook-ph']['form']['tel'] = array(
    '#type' => 'textarea',
    '#title' => t('Phone'),
    '#rows' => 2,
    '#default_value' => '',
  );
  $form['ovsz-phonebook-ph']['form']['mob'] = array(
    '#type' => 'textarea',
    '#title' => t('Mobile'),
    '#rows' => 2,
    '#default_value' => '',
  );
  $form['ovsz-phonebook-ph']['form']['fax'] = array(
    '#type' => 'textarea',
    '#title' => t('Fax'),
    '#rows' => 2,
    '#default_value' => '',
  );
  $form['ovsz-phonebook-ph']['form']['add'] = array(
    '#markup' => '<img id="ovsz-phonebook-form-modify" class="ovsz-phonebook-edit-submit" src="' . $p . '/icon-save-modify.png" />' .
      '<img id="ovsz-phonebook-form-add" class="ovsz-phonebook-edit-submit" src="' . $p . '/icon-save-add.png" />' .
      '<img id="ovsz-phonebook-form-remove" class="ovsz-phonebook-edit-submit" src="' . $p . '/icon-remove.png" />',
  );
  $form['ovsz-phonebook-ph']['title2'] = array(
    '#markup' => '</div>',
  );
  $form['ovsz-phonebook-ph']['markup'] = array(
    '#markup' => ovsz_phonebook_render(),
  );

/*
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('ovsz_phonebook_admin_ph_submit')
  );
*/
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'ovsz_phonebook') . '/ovsz_phonebook.phone.js',
  );
  $form['#attached']['js'][] = array(
    'data' => array('ovsz_phonebook' => array(
      'edit_url' => $base_url . '/?q='. 'admin/config/ovsz-phonebook/phone-edit',
    )),
    'type' => 'setting'
  );

  return $form;
}

  drupal_json_output(array('id' => $id, 'data' => $data, 'error' => $error));
  exit();
}
