(function ($) {

Drupal.behaviors.ovsz_phonebook = {

  attach: function(context, settings) {
    $('body').once('ovsz_phonebook', function () {

      $('table.ovsz-phonebook .control th > img.col').click(function(e) {
        var l = $(this).parent().attr('class');
        if (l == 'level-all') l = ''; else l = '.'+ l;
        $('table.ovsz-phonebook .folder' + l).removeClass('collapsed');
        $('table.ovsz-phonebook .folder' + l).parent().parent().find('> tbody').removeClass('collapsed');
        $('table.ovsz-phonebook .folder' + l).click();
      });

      $('table.ovsz-phonebook .control th > img.exp').click(function(e) {
        var l = $(this).parent().attr('class');
        if (l == 'level-all') l = ''; else l = '.'+ l;
        $('table.ovsz-phonebook .folder' + l).addClass('collapsed');
        $('table.ovsz-phonebook .folder' + l).parent().parent().find('> tbody').addClass('collapsed');
        $('table.ovsz-phonebook .folder' + l).click();
      });

      $('table.ovsz-phonebook .control th > img.nodatacol').click(function(e) {
        $('table.ovsz-phonebook tr.nodata').hide();
      });

      $('table.ovsz-phonebook .control th > img.nodataexp').click(function(e) {
        $('table.ovsz-phonebook tr.nodata').show();
      });

      // Expand/collapse sub-folder when clicking parent folder.
      $('table.ovsz-phonebook .folder').click(function(e) {
        var el = $(this).parent().parent().find('tbody').first();
        // Expand.
        if ($(el).hasClass('collapsed')) {
          $(el).removeClass('collapsed');
          $(this).removeClass('collapsed');
        }
        // Collapse.
        else {
          $(el).addClass('collapsed');
          $(this).addClass('collapsed');
        }
        // Prevent collapsing parent folders.
        return false;
      });

    });
  }
};

})(jQuery);