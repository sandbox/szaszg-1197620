<?php

/**
 * create a test table
 */

function tableutils_edit_testpage() {
  $def_id =  OCdrupal_json_encode(array(
    'id' => array(
      'type' => 'hidden',
      'prep' => NULL,
    ),
    'org' => array(
      'type' => 'select',
      'prep' => NULL,
      'list' => array(
        0 => 'Org1', 1=> 'Org2', 2 => 'Org3'
      ),
    )
  ));
  $table = 
    '' .
    '<table class="tableutils-edit"><thead>' .
      '<tr><th><a class="tableutils-edit" rel="' . . '"><a class="tableutils-edit" rel="">Org</th>' .
        '<th><a class="tableutils-edit" rel="">Name</th>' .
        '<th><a class="tableutils-edit" rel="">email</th>' .
        '<th><a class="tableutils-edit" rel="">Tel</th>' .
        '<th><a class="tableutils-edit" rel="">Addr</th></tr>' .
      '</thead><tbody>' .

      '<tr><td><a class="tableutils-edit" rel="">org1</td>' .
        '<td><a class="tableutils-edit" rel="">name1</td>' .
        '<td><a class="tableutils-edit" rel="">email@1</td>' .
        '<td><a class="tableutils-edit" rel="">Telephone1</td>' .
        '<td><a class="tableutils-edit" rel="">Address1</td></tr>' .

      '<tr><td><a class="tableutils-edit" rel="">org1</td>' .
        '<td><a class="tableutils-edit" rel="">name1</td>' .
        '<td><a class="tableutils-edit" rel="">email@1</td>' .
        '<td><a class="tableutils-edit" rel="">Telephone1</td>' .
        '<td><a class="tableutils-edit" rel="">Address1</td></tr>' .

       '</tbody></table>' .
    '';

  return $table;
}
