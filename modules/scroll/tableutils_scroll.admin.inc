<?php

/**
 * @file
 * Administration page callbacks for the Table collapse module.
 */

/**
 * Display the Table Collapse settings form.
 */
function tableutils_scroll_admin_settings_form($form_state) {
  $form = array();

//  $settings = variable_get('tableutils_scroll', array());

  $data['activation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activation'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $data['activation']['selector'] = array(
    '#type' => 'textarea',
    '#title' => t('jQuery selector'),
    '#default_value' => isset($settings['activation']['selector']) ? $settings['activation']['selector'] : '',
  );
  $data['activation']['activation_type'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Fancybox on specific pages'),
    '#options' => array(
      'exclude' => t('Enable on every page except the listed pages.'),
      'include' => t('Enable on only the listed pages.'),
    ),
    '#default_value' => isset($settings['activation']['activation_type']) ? $settings['activation']['activation_type'] : 'exclude',
  );
  if (user_access('use PHP for block visibility')) {
    $data['activation']['activation_type']['#options']['php'] = t('Enable if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
  }
  $data['activation']['activation_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => isset($settings['activation']['activation_pages']) ? $settings['activation']['activation_pages'] : "admin*\nimg_assist*\nnode/add/*\nnode/*/edit",
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );

  $form['data'] = $data;
  $form['data']['#tree'] = TRUE;

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );

  return $form;
}
