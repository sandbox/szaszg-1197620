(function ($) {

Drupal.behaviors.tableutils_scroll = {
  _scrollbarWidth: 0,
  _displayHeight: 0,

  scrollbarWidth: function() {
    if (!Drupal.behaviors.tableutils_scroll._scrollbarWidth) {
      var $body = $('body');
      var w = $body.css('overflow', 'hidden').width();
      $body.css('overflow','scroll');
      w -= $body.width();
      if (!w) w=$body.width()-$body[0].clientWidth; // IE in standards mode
      $body.css('overflow','');
      Drupal.behaviors.tableutils_scroll._scrollbarWidth = w;
    }
    return Drupal.behaviors.tableutils_scroll._scrollbarWidth;
  },
  displayHeight: function() {
    if (!Drupal.behaviors.tableutils_scroll._displayHeight) {
      if (typeof(window.innerWidth) != 'undefined')
        Drupal.behaviors.tableutils_scroll._displayHeight = window.innerHeight;
      else if (typeof(document.documentElement) != 'undefined'
        && typeof(document.documentElement.clientWidth) !=
        'undefined' && document.documentElement.clientWidth != 0)
        Drupal.behaviors.tableutils_scroll._displayHeight = document.documentElement.clientHeight;
      else
        Drupal.behaviors.tableutils_scroll._displayHeight = document.getElementsByTagName('body')[0].clientHeight;
    }
    return Drupal.behaviors.tableutils_scroll._displayHeight;
  },
  attach: function(context, settings) {
    $('table').each( function (index, table) {

      var head;
      var body = $(table).find("tbody");
      var foot = $(table).find("tfoot");
      var widths = []; var last = 0;
      if (!(table.tHead))
        $(table).prepend($('<thead></thead>').append($(table).find("tr:first").remove()));
      head = $(table).find("thead");

      $(head).find('tr:first > td, tr:first > th').each(function(i) {
        widths[i] = $(this).width();
      });
      var w1 = $(body).find('tr:first').innerWidth();
      $(head).css({display: 'block'});
      $(body).css({display: 'block', 'overflow-y': 'auto'});
      $(foot).css({display: 'block'});

//TODO: colspan
      $(body).find('tr:first > td, tr:first > th').each(function(i) {
        $(this).width(widths[i]);
        last = i;
      });
//TODO: height
      $(body).css({'max-height': parseInt(Drupal.behaviors.tableutils_scroll.displayHeight() * 0.67)});
      var w2 = $(body).find('tr:first').innerWidth();
      if (w2 + 5 < w1) widths[last] += Drupal.behaviors.tableutils_scroll.scrollbarWidth();
      $(head).find('tr:first > td, tr:first > th').each(function(i) {
        $(this).width(widths[i]);
      });
      $(foot).find('tr:first > td, tr:first > th').each(function(i) {
        $(this).width(widths[i]);
      });
    });
  }
};

})(jQuery);